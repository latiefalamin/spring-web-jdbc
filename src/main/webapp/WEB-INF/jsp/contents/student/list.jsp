<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            Student List
            <a href="<c:url value='/student/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>
    <table class="table table-striped">
        <thead>
        <td>Id</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Address</td>
        <td>Address</td>
        <td>Sex</td>
        <td>Action</td>
        </thead>
        <tbody>
        <c:forEach items="${studentList}" var="student">
            <tr>
                <td><a href="<c:url value='/student?id=${student.id}'/>">${student.id}</a></td>
                <td>${student.firstName}</td>
                <td>${student.lastName}</td>
                <td>${student.address}</td>
                <td>${student.sex}</td>
                <td>${student.age}</td>
                <td>
                    <a href="<c:url value='/student/form?id=${student.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/student/delete?id=${student.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>
