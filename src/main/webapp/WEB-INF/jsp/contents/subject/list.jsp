<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            Subject List
            <a href="<c:url value='/subject/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>
    <table class="table table-striped">
        <thead>
        <td>Id</td>
        <td>Code</td>
        <td>Name</td>
        <td>Sks</td>
        <td>Action</td>
        </thead>
        <tbody>
        <c:forEach items="${subjectList}" var="subject">
            <tr>
                <td><a href="<c:url value='/subject?id=${subject.id}'/>">${subject.id}</a></td>
                <td>${subject.code}</td>
                <td>${subject.name}</td>
                <td>${subject.sks}</td>
                <td>
                    <a href="<c:url value='/subject/form?id=${subject.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/subject/delete?id=${subject.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>
