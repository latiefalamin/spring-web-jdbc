<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav">
                <li class="${menuHomeClass}"><a href="<c:url value='/'/>">Home</a></li>
                <li class="${menuStudentClass }"><a href="<c:url value='/student'/>">Student</a></li>
                <li class="${menuSubjectClass }"><a href="<c:url value='/subject'/>">Subject</a></li>
            </ul>
        </div>
    </div>
</div>