package com.secondstack.training.spring.jdbc.controller.web;

import com.secondstack.training.spring.jdbc.domain.Student;
import com.secondstack.training.spring.jdbc.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("student")
public class StudentController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form student");

        Student student = new Student();
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("studentUrl", "/student");
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("student") Student student, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to create student");

        studentService.save(student);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(value = "form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to get form student");

        Student student = studentService.findById(id);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("studentUrl", "/student?id=" + id);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-form-tiles";
    }

    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id")Integer id, @ModelAttribute("student") Student student, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to update student");

        studentService.update(id, student);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap) throws SQLException{
        logger.debug("Received request to get list student");

        List<Student> studentList = studentService.findAll();
        modelMap.addAttribute("studentList", studentList);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
        logger.debug("Received request to get data student");

        Student student = studentService.findById(id);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(value = "delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
        logger.debug("Received request to delete student");

        studentService.delete(id);

        return "redirect:/student";
    }

}
