package com.secondstack.training.spring.jdbc.service;

import com.secondstack.training.spring.jdbc.domain.Student;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StudentService {

    int save(Student student) throws SQLException;
    int update(Integer id, Student student) throws SQLException;
    int delete(Student student) throws SQLException;
    int delete(Integer id) throws SQLException;
    List<Student> findAll() throws SQLException;
    Student findById(Integer id) throws SQLException;
}
