package com.secondstack.training.spring.jdbc.service.impl;

import com.secondstack.training.spring.jdbc.domain.Subject;
import com.secondstack.training.spring.jdbc.domain.enumeration.Sex;
import com.secondstack.training.spring.jdbc.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private DataSource dataSource;

    @Override
    public int save(Subject subject) throws SQLException {
        String sql = "INSERT INTO subject(code, name, sks) VALUE(?,?,?)";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setString(1, subject.getCode());
        statement.setString(2, subject.getName());
        statement.setInt(3, subject.getSks() == null ? 0 : subject.getSks());

        return statement.executeUpdate();
    }

    @Override
    public int update(Integer id, Subject subject) throws SQLException {
        String sql = "UPDATE subject s SET code = ?, name = ?, sks = ? WHERE subject_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setString(1, subject.getCode());
        statement.setString(2, subject.getName());
        statement.setInt(3, subject.getSks() == null ? 0 : subject.getSks());
        statement.setInt(4, id);
        return statement.executeUpdate();
    }

    @Override
    public int delete(Subject subject) throws SQLException {
        return delete(subject.getId());
    }

    @Override
    public int delete(Integer id) throws SQLException {
        String sql = "DELETE FROM subject WHERE subject_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setInt(1, id);
        return statement.executeUpdate();
    }

    @Override
    public List<Subject> findAll() throws SQLException {
        String sql =
                "SELECT s.subject_id, s.code, s.name, s.sks "
                        + " FROM subject s "
                        + " ORDER BY s.subject_id";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        List<Subject> result = new ArrayList<Subject>();
        while (rs.next()) {
            Subject s = new Subject();
            s.setId(rs.getInt("subject_id"));
            s.setCode(rs.getString("code"));
            s.setName(rs.getString("name"));
            s.setSks(rs.getInt("sks"));
            result.add(s);
        }
        return result;
    }

    @Override
    public Subject findById(Integer id) throws SQLException {
        String sql =
                "SELECT s.subject_id, s.code, s.name, s.sks "
                        + " FROM subject s "
                        + " where s.subject_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            Subject s = new Subject();
            s.setId(rs.getInt("subject_id"));
            s.setCode(rs.getString("code"));
            s.setName(rs.getString("name"));
            s.setSks(rs.getInt("sks"));
            return s;
        } else {
            return null;
        }
    }
}
