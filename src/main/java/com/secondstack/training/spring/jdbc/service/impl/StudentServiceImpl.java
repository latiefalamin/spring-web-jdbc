package com.secondstack.training.spring.jdbc.service.impl;

import com.secondstack.training.spring.jdbc.domain.Student;
import com.secondstack.training.spring.jdbc.domain.enumeration.Sex;
import com.secondstack.training.spring.jdbc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private DataSource dataSource;

    @Override
    public int save(Student student) throws SQLException {
        String sql = "INSERT INTO student(first_name, last_name, address, sex, age) VALUE(?,?,?,?, ?) ";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setString(1, student.getFirstName());
        statement.setString(2, student.getLastName());
        statement.setString(3, student.getAddress());
        statement.setString(4, student.getSex() + "");
        statement.setInt(5, student.getAge() == null ? 0 : student.getAge());

        return statement.executeUpdate();
    }

    @Override
    public int update(Integer id, Student student) throws SQLException {
        String sql = "UPDATE student p SET first_name = ?, last_name = ?, address = ?, sex = ?, age = ? WHERE student_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setString(1, student.getFirstName());
        statement.setString(2, student.getLastName());
        statement.setString(3, student.getAddress());
        statement.setString(4, student.getSex() + "");
        statement.setInt(5, student.getAge() == null ? 0 : student.getAge());
        statement.setInt(6, id);
        return statement.executeUpdate();
    }

    @Override
    public int delete(Student student) throws SQLException {
        return delete(student.getId());
    }

    @Override
    public int delete(Integer id) throws SQLException {
        String sql = "DELETE FROM student WHERE student_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setInt(1, id);
        return statement.executeUpdate();
    }

    @Override
    public List<Student> findAll() throws SQLException {
        String sql =
                "SELECT p.student_id, p.first_name, p.last_name, p.address, p.sex, p.age "
                        + " FROM student p "
                        + " ORDER BY p.student_id";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        List<Student> result = new ArrayList<Student>();
        while (rs.next()) {
            Student p = new Student();
            p.setId(rs.getInt("student_id"));
            p.setFirstName(rs.getString("first_name"));
            p.setLastName(rs.getString("last_name"));
            p.setAddress(rs.getString("address"));
            p.setSex(Sex.valueOf(rs.getString("sex")));
            p.setAge(rs.getInt("age"));
            result.add(p);
        }
        return result;
    }

    @Override
    public Student findById(Integer id) throws SQLException {
        String sql =
                "SELECT p.student_id, p.first_name, p.last_name, p.address, p.sex, p.age "
                        + " FROM student p "
                        + " where p.student_id = ?";
        PreparedStatement statement = dataSource.getConnection().prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            Student p = new Student();
            p.setId(rs.getInt("student_id"));
            p.setFirstName(rs.getString("first_name"));
            p.setLastName(rs.getString("last_name"));
            p.setAddress(rs.getString("address"));
            p.setSex(Sex.valueOf(rs.getString("sex")));
            p.setAge(rs.getInt("age"));
            return p;
        } else {
            return null;
        }
    }
}
