package com.secondstack.training.spring.jdbc.service;

import com.secondstack.training.spring.jdbc.domain.Subject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface SubjectService {

    int save(Subject subject) throws SQLException;
    int update(Integer id, Subject subject) throws SQLException;
    int delete(Subject subject) throws SQLException;
    int delete(Integer id) throws SQLException;
    List<Subject> findAll() throws SQLException;
    Subject findById(Integer id) throws SQLException;
}
